<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Model;

use Kuti\PostPoint\Data\PostPoint;
use Kuti\PostPoint\Enum\Post;

/**
 * Class DataExtractor
 *
 * @package Kuti\PostPoint\Model
 */
class DataExtractor
{
    /**
     * @param array $source
     *
     * @return PostPoint[]
     */
    public function extract(array $source): array
    {
        $data = [];
        foreach ((array)$source[Post::ITEMS_KEY] as $element) {
            $data[] = $this->extractOne($element);
        }

        return $data;
    }

    /**
     * @param array $element
     *
     * @return PostPoint
     */
    public function extractOne(array $element): PostPoint
    {
        return new PostPoint(
            (int)$element[Post::ID],
            (string)$element[Post::TYPE],
            (int)$element[Post::ZIP_CODE],
            (string)$element[Post::CITY],
            (string)$element[Post::ADDRESS],
            (string)$element[Post::NAME],
            (string)$element[Post::STATUS],
            (int)$element[Post::CLOSE_AT],
            (bool)$element[Post::REALTIME],
            (bool)$element[Post::HAS_BOOK_APPOINTMENT],
            (bool)$element[Post::PO_BOX],
            (float)$element[Post::LAT],
            (float)$element[Post::LNG]
        );
    }
}