<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Model;

/**
 * Class Downloader
 *
 * @package Kuti\PostPoint\Model
 */
class Downloader
{
    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     * @throws \Exception
     */
    public function getUrl(): string
    {
        if (!$this->url) {
            throw new \Exception('Url not set for Downloader');
        }

        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function downloadAndDecode()
    {
        return json_decode($this->getContents(), true);
    }

    /**
     * @return string
     */
    private function getContents()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = (string)curl_exec($curl);
        curl_close($curl);

        return $data;
    }
}