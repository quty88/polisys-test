<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Data;

/**
 * Class PostPoint
 *
 * @package Kuti\PostPoint\Data
 */
class PostPoint
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $zipCode;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $closeAt;

    /**
     * @var bool
     */
    private $realtime;

    /**
     * @var bool
     */
    private $hasBookAppointment;

    /**
     * @var bool
     */
    private $poBox;

    /**
     * @var float
     */
    private $lat;

    /**
     * @var float
     */
    private $lng;

    /**
     * PostPoint constructor.
     *
     * @param int    $id
     * @param string $type
     * @param int    $zipCode
     * @param string $city
     * @param string $address
     * @param string $name
     * @param string $status
     * @param int    $closeAt
     * @param bool   $realtime
     * @param bool   $hasBookAppointment
     * @param bool   $poBox
     * @param float  $lat
     * @param float  $lng
     */
    public function __construct(
        int $id,
        string $type,
        int $zipCode,
        string $city,
        string $address,
        string $name,
        string $status,
        int $closeAt,
        bool $realtime,
        bool $hasBookAppointment,
        bool $poBox,
        float $lat,
        float $lng
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->address = $address;
        $this->name = $name;
        $this->status = $status;
        $this->closeAt = $closeAt;
        $this->realtime = $realtime;
        $this->hasBookAppointment = $hasBookAppointment;
        $this->poBox = $poBox;
        $this->lat = $lat;
        $this->lng = $lng;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getZipCode(): int
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getCloseAt(): int
    {
        return $this->closeAt;
    }

    /**
     * @return bool
     */
    public function isRealtime(): bool
    {
        return $this->realtime;
    }

    /**
     * @return bool
     */
    public function isHasBookAppointment(): bool
    {
        return $this->hasBookAppointment;
    }

    /**
     * @return bool
     */
    public function isPoBox(): bool
    {
        return $this->poBox;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

}