define([
    'jquery',
    'underscore',
    'uiComponent',
    'ko'
], function ($, _, Component, ko) {
    return Component.extend({
        defaults: {
            template: 'Kuti_PostPoint/postpoint'
        },
        selectedPostPoint: ko.observable()
    });
});