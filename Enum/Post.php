<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Enum;

/**
 * Class Post
 *
 * @package Kuti\PostPoint\Enum
 */
final class Post
{
    const POST_SRV_URL          = 'https://www.posta.hu/szolgaltatasok/posta-srv-postoffice/rest/postoffice/list';
    const POST_POINT_SOURCE_URL = self::POST_SRV_URL . '?searchField=&searchText=&types=postapoint';

    const ITEMS_KEY = 'items';

    const ID                   = 'id';
    const TYPE                 = 'type';
    const ZIP_CODE             = 'zipCode';
    const CITY                 = 'city';
    const ADDRESS              = 'address';
    const NAME                 = 'name';
    const STATUS               = 'status';
    const CLOSE_AT             = 'closeAt';
    const REALTIME             = 'realtime';
    const HAS_BOOK_APPOINTMENT = 'hasBookAppointment';
    const PO_BOX               = 'poBox';
    const LAT                  = 'lat';
    const LNG                  = 'lng';
}