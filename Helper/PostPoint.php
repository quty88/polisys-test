<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Helper;

use Kuti\PostPoint\Enum\Post;
use Kuti\PostPoint\Model\DataExtractor;
use Kuti\PostPoint\Model\Downloader;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class PostPoint
 *
 * @package Kuti\PostPoint\Helper
 */
class PostPoint extends AbstractHelper
{
    /**
     * @var DataExtractor
     */
    private $dataExtractor;

    /**
     * @var Downloader
     */
    private $downloader;

    /**
     * PostPoint constructor.
     *
     * @param Context       $context
     * @param DataExtractor $dataExtractor
     * @param Downloader    $downloader
     */
    public function __construct(
        Context $context,
        DataExtractor $dataExtractor,
        Downloader $downloader
    ) {
        parent::__construct($context);
        $this->dataExtractor = $dataExtractor;
        $this->downloader = $downloader;
    }

    /**
     * @return array|\Kuti\PostPoint\Data\PostPoint[]
     */
    public function getPostPoints()
    {
        $this->downloader->setUrl(Post::POST_POINT_SOURCE_URL);

        return $this->dataExtractor->extract($this->downloader->downloadAndDecode());
    }
}