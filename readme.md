# Postpoint test module

## Installation
 - clone the module to app/code/Kuti/PostPoint directory
 - php bin/magento module:enable Kuti_PostPoint
 - php bin/magento se:up
 - php bin/magento se:di:co
 - php bin/magento se:stat:dep
 
## Usage
The postpoint list url is /postpoint, this will list the available postpoints in a select field :)
