<?php

declare(strict_types=1);

namespace Kuti\PostPoint\Block;

use Kuti\PostPoint\Data\PostPoint;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;

/**
 * Class PostPointList
 *
 * @package Kuti\PostPoint\Block
 */
class PostPointList extends Template
{
    /**
     * @var Json
     */
    private $jsonSerializer;

    /**
     * @var PostPoint[]
     */
    private $postPoints;

    /**
     * @var \Kuti\PostPoint\Helper\PostPoint
     */
    private $postPointHelper;

    /**
     * PostPointList constructor.
     *
     * @param Template\Context                 $context
     * @param Json                             $jsonSerializer
     * @param \Kuti\PostPoint\Helper\PostPoint $postPointHelper
     * @param array                            $data
     *
     * @internal param DataExtractor $dataExtractor
     * @internal param Downloader $downloader
     */
    public function __construct(
        Template\Context $context,
        Json $jsonSerializer,
        \Kuti\PostPoint\Helper\PostPoint $postPointHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsonSerializer = $jsonSerializer;
        $this->postPointHelper = $postPointHelper;
    }

    /**
     * @return bool|string
     * @throws \InvalidArgumentException
     */
    public function getJsonConfig()
    {
        $points = [];
        foreach ($this->getPostPoints() as $postPoint) {
            $points[] = $this->getPostPointData($postPoint);
        }

        return $this->jsonSerializer->serialize($points);
    }

    /**
     * @return array|\Kuti\PostPoint\Data\PostPoint[]
     */
    public function getPostPoints()
    {
        if ($this->postPoints) {
            return $this->postPoints;
        }
        $this->postPoints = $this->postPointHelper->getPostPoints();

        return $this->postPoints;
    }

    /**
     * @param PostPoint $postPoint
     *
     * @return array
     */
    private function getPostPointData($postPoint)
    {
        return [
            'id'       => $postPoint->getId(),
            'name'     => $postPoint->getName(),
            'zip_code' => $postPoint->getZipCode(),
            'city'     => $postPoint->getCity(),
            'address'  => $postPoint->getAddress()
        ];
    }
}